const fs = require("fs");

const REACT_PREFIX = "REACT_APP_";
// const ENV_PREFIX = "REACT_APP_ENV";

// Apply environment variables to build folder
if (fs.existsSync("./build")) {
  //  Process Environment variables
  let envConfig = {
    // env without REACT_PREFIX
    COMMON_PUBLIC_FRONTEND_ROOT_PATH: process.env.COMMON_PUBLIC_FRONTEND_ROOT_PATH || "",
    REGION_LANGUAGE: process.env.REGION_LANGUAGE || "",
    PRUFORCE_DEFAULT_LANGUAGE: process.env.PRUFORCE_DEFAULT_LANGUAGE || "",
    REACT_APP_ENV: process.env.REACT_APP_ENV || "",
  };
  // env with REACT_PREFIX
  for (const key in process.env) {
    if (key.includes(REACT_PREFIX)) {
      envConfig[key] = process.env[key];
    }
  }

  let appendStr = `window.envConfig = ${JSON.stringify(envConfig)};`;
  if (!fs.existsSync("./build/dist")) {
    fs.mkdirSync("./build/dist");
  }
  fs.writeFileSync("./build/dist/runtime-env-config.js", appendStr);
}

// Apply environment variables from .env file (Local only)
if (fs.existsSync("./.env")) {
  let envConfig = {};
  let env = fs.readFileSync("./.env", "utf8");
  let rows = env.split("\n");
  rows.forEach((row) => {
    let value = row.substring(row.indexOf("=") + 1);
    let values = row.split("=");
    envConfig[values[0]] = value;
  });
  let appendStr = `window.envConfig = ${JSON.stringify(envConfig)};`;
  if (!fs.existsSync("./public/dist")) {
    fs.mkdirSync("./public/dist");
  }
  fs.writeFileSync("./public/dist/runtime-env-config.js", appendStr);
  // for local build
  // if (fs.existsSync("./build/dist")) {
  //   fs.writeFileSync("./build/dist/runtime-env-config.js", appendStr);
  // }
} else {
  let appendStr = `window.envConfig = {}`;
  if (!fs.existsSync("./public/dist")) {
    fs.mkdirSync("./public/dist");
  }
  fs.writeFileSync("./public/dist/runtime-env-config.js", appendStr);
}
