import React, { FC, useEffect, useState } from 'react';
import 'survey-react/survey.min.css';
import 'survey-core/defaultV2.css';
import { StylesManager, Model } from 'survey-core';
import { Survey } from 'survey-react-ui';
import showdown from 'showdown';
import { CompletedPage } from '../component/completed';
import { ActivityLogAction, SurveyProps, SurveyStatusProps, SubmitSurveyResult } from './types/surveyTypes';
import { submitSurveyItem, windowEvent, fetchWindowEvent } from './network/crud';
import { LoadingComponent } from '../component/loading';
import { ShowErrorComponent } from '../component/showError';
import { AgentInformation } from '../component/agentInfomation';

StylesManager.applyTheme('defaultV2');

type SurveySubmitFormProps = {
  surveyResult: SurveyProps;
};

const surveyStatus: SurveyStatusProps = {
  activityLogAction: ActivityLogAction.viewed,
  activityLogLastQuestion: undefined,
};

const SurveySubmitForm: FC<SurveySubmitFormProps> = ({ surveyResult }) => {
  const pageIndex = surveyResult.surveyBody.pages.length - 1;
  const [surveyLength] = useState(surveyResult.surveyBody.pages[pageIndex].elements?.length);
  const [isCompleted, setIsCompleted] = useState<boolean>(false);
  const [submitResult, setSubmitResult] = useState<SubmitSurveyResult>();
  const [loadingDisplayMode, setLoadingDisplayMode] = useState<'none' | 'flex'>('none');
  const [errorDisplayMode, setErrorDisplayMode] = useState<'none' | 'flex'>('none');
  const pageHeight = window.innerHeight * (window.visualViewport?.scale || 1);

  let tncCount = 0;
  let linkArray = [];
  if (surveyResult.consents) {
    for (const tncLink of surveyResult.consents) {
      tncCount++;
      linkArray.push({
        value: `tncLink${tncCount}`,
        text: tncLink,
      });
    }
    if (surveyResult.surveyBody.pages[pageIndex].elements?.length === surveyLength) {
      surveyResult.surveyBody.pages[pageIndex].elements.push({
        type: 'checkbox',
        name: 'tncLink',
        choices: linkArray,
        titleLocation: 'hidden',
        validators: [
          {
            type: 'expression',
            text: 'Please agree to the terms and conditions',
            expression: `{tncLink.length} = ${linkArray.length}`,
          },
        ],
      });
    }
  }

  const model = new Model(surveyResult.surveyBody);

  model.css = {
    ...model.css,
    saveData: {
      ...model.css.saveData,
      error: 'saveErrorContainer',
      saveAgainButton: 'saveAgainBtn',
    },
    navigation: {
      prev: 'sd-navigation__prev-btn',
    },
  };

  model.autoGrowComment = true;
  model.completeText = 'Submit';

  model.onTextMarkdown.add((survey: any, options: any) => {
    const converter = new showdown.Converter({ openLinksInNewWindow: true });
    if (options.text.includes('[') && options.text.includes(']')) {
      const str = converter.makeHtml(options.text);
      options.html = str.substring(3).substring(0, str.length - 4);
    }
  });

  let questionCount = 1;
  model.onUpdateQuestionCssClasses.add((survey: any, options: any) => {
    if (questionCount === surveyLength + 1) {
      options.cssClasses.label += ' tnc';
      options.cssClasses.mainRoot += ' tncRoot';
    } else {
      questionCount++;
    }
  });

  model.onAfterRenderQuestionInput.add((survey: any, options: any) => {
    if (options.question.getType() === 'comment') {
      options.htmlElement.rows = 1;
    } else if (options.question.getType() === 'file') {
      options.question.chooseButtonCaption = 'Choose File';
      options.question.cleanButtonCaption = 'Delete File';
    }
  });

  model.onComplete.add(async (survey: any) => {
    setLoadingDisplayMode('flex');
    const data = survey.data;
    try {
      if (
        !surveyResult.consents ||
        (survey.data.tncLink &&
          survey.data.tncLink.length === (surveyResult.consents ? surveyResult.consents.length : 0))
      ) {
        delete data.tncLink;
        const submitResult = await submitSurveyItem(surveyResult.surveyId, data);
        setSubmitResult(submitResult);
        setIsCompleted(submitResult.isCompleted);
      }
    } catch (err: any) {
      setLoadingDisplayMode('none');
      setErrorDisplayMode('flex');
      setTimeout(() => {
        setErrorDisplayMode('none');
      }, 2000);
      console.log(`err: ${JSON.stringify(err)}`);
    } finally {
      setTimeout(() => {
        setLoadingDisplayMode('none');
      }, 2000);
    }
  });

  const handleViewLog = async () => {
    await windowEvent(surveyResult.surveyId, surveyStatus);
  };

  useEffect(() => {
    handleViewLog();

    let userAgent = navigator.userAgent;

    const handleTabClose = async (event: any) => {
      event.preventDefault();

      if (!isCompleted) {
        const anserQuestion = model.data;
        delete anserQuestion.tncLink;
        const questionTitle = Object.keys(anserQuestion);

        if (questionTitle.length === 0) {
          surveyStatus.activityLogAction = ActivityLogAction.closedWithoutAnswer;
        } else {
          surveyStatus.activityLogAction = ActivityLogAction.answered;
          const question = model.getQuestionByName(questionTitle[questionTitle.length - 1]);
          surveyStatus.activityLogLastQuestion = question.title;
        }

        await fetchWindowEvent(surveyResult.surveyId, surveyStatus);
      }
    };
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent)) {
      window.addEventListener('blur', handleTabClose);
      return () => {
        window.removeEventListener('blur', handleTabClose);
      };
    } else {
      window.addEventListener('unload', handleTabClose);

      return () => {
        window.removeEventListener('unload', handleTabClose);
      };
    }
  });

  return (
    <>
      {isCompleted ? (
        <CompletedPage
          content={submitResult?.completeMessage.content || undefined}
          title={submitResult?.completeMessage.title || undefined}
        />
      ) : (
        <div style={{ display: 'flex', flexDirection: 'column', width: '100%', height: '100%' }}>
          <AgentInformation
            agentName={surveyResult.agentProfile.displayName}
            agentTitle={surveyResult.agentProfile.designationName}
          />
          <div style={{ boxShadow: '0px 4px 10px #00000019' }}>
            <div
              style={{ width: '100%', height: pageHeight < 1180 ? (pageHeight < 667 ? '130px' : '180px') : '386px' }}
            >
              <img
                alt=""
                src={surveyResult.banner || `${require('../../images/default_banner.png')}`}
                style={{
                  width: '100%',
                  height: '100%',
                  objectFit: 'cover',
                }}
              />
            </div>
            <div style={{ padding: '8px 16px 0', backgroundColor: 'white' }}>
              <img alt="" src={`${require('../../images/logo.png')}`} style={{ height: '20px' }} />
            </div>
            <div style={{ minHeight: 'max-content', overflowX: 'hidden' }}>
              <Survey model={model} />
              <LoadingComponent displayMode={loadingDisplayMode} />
              <ShowErrorComponent displayMode={errorDisplayMode} />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default SurveySubmitForm;
