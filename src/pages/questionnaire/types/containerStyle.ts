import { styled } from '@material-ui/core';

export const Container = styled('div')({
  display: 'grid',
  gridTemplateRows: 'auto 1fr',
});

export const QuestionnaireContainer = styled('div')({
  position: 'relative',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'column',
});

export const LoadingContainer = styled('div')({

  width: '94px',

  height: '88px',

  position: 'fixed',

  top: 0,

  bottom: 0,

  right: 0,

  left: 0,

  margin: 'auto',

  background: 'rgba(0, 0, 0, 0.8)',

  borderRadius: '12px',

});



export const ErrorContainer = styled('div')({

  width: '332px',

  height: '52px',

  position: 'fixed',

  top: 0,

  bottom: 0,

  right: 0,

  left: 0,

  margin: 'auto',

  padding: '10px 16px',

  background: 'rgba(0, 0, 0, 0.8)',

  borderRadius: '12px',

});