export type CompleteMessage = {
  content: string | undefined;
  title: string | undefined;
};

export type GetSurveyResult = {
  surveyImage?: string;
  body: Record<string, any>;
  isCompleted: boolean;
  consents?: Array<string>;
  completeMessage: CompleteMessage;
  agentProfile: {
    displayName: string;
    designationName: string;
  };
};

export type SubmitSurveyResult = {
  body: Record<string, any>;
  isCompleted: boolean;
  completeMessage: CompleteMessage;
};

export type LogSurveyResult = {
  body: any;
  isCompleted: boolean;
};

export type SurveyProps = {
  surveyId: string;
  surveyBody: any;
  banner?: string;
  consents?: Array<string>;
  agentProfile: {
    displayName: string;
    designationName: string;
  };
};

export enum ActivityLogAction {
  viewed = 'viewed',
  closedWithoutAnswer = 'closedWithoutAnswer',
  answered = 'answered',
}

export type SurveyStatusProps = {
  activityLogAction: ActivityLogAction;
  activityLogLastQuestion?: string;
};
