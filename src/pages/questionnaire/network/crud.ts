import { GetSurveyResult, LogSurveyResult, SubmitSurveyResult, SurveyStatusProps } from '../types/surveyTypes';
import ResourceService from '../../../api/apiClient';

const getSurveyDataUrl = '/surveys/';

export const getSurveyItem = async (id: string): Promise<GetSurveyResult> => {
  return await ResourceService<GetSurveyResult>(false, getSurveyDataUrl + id);
};

export const submitSurveyItem = async (surveyId: string, data: any): Promise<SubmitSurveyResult> => {
  const bodyData = { body: data };

  return await ResourceService<SubmitSurveyResult>(true, getSurveyDataUrl + surveyId, bodyData);
};

export const windowEvent = async (surveyId: string, data: SurveyStatusProps): Promise<LogSurveyResult> => {
  const bodyData = data;

  return await ResourceService<LogSurveyResult>(true, `${getSurveyDataUrl}${surveyId}/log`, bodyData);
};

export const fetchWindowEvent = async (surveryId: string, data: SurveyStatusProps) => {
  let eventUrl = `${window.location.origin}${getSurveyDataUrl}${surveryId}/log`;
  let url = new window.URL(eventUrl);
  fetch(url.href, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: { 'Content-Type': 'application/json' },
    keepalive: true,
  });
};
