import React, { useCallback, useEffect, useState } from 'react';
import { GetSurveyResult } from './types/surveyTypes';
import { useParams } from 'react-router-dom';
import { getSurveyItem } from './network/crud';
import SurveySubmitForm from './SurveySubmitForm';
import { CompletedPage } from '../component/completed';
import { ErrorPage } from '../component/errorPage';
import { LoadingComponent } from '../component/loading';
import { Seo } from '../component/seo';

export const QuestionnairePage = () => {
  const [surveyResult, setSurveyResult] = useState<GetSurveyResult>();

  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isError, setIsError] = useState<boolean>(false);
  const { surveyId } = useParams<{ surveyId: string }>();
  const reloadData = useCallback(() => {
    if (surveyId) {
      getSurveyItem(surveyId)
        .then(result => {
          console.log(result);
          setIsLoading(false);
          setSurveyResult(result);
        })
        .catch(error => {
          setIsError(true);
          setIsLoading(false);
          console.log(error);
        });
    }
  }, [surveyId]);

  console.log('sdasdasdasd');
  useEffect(() => {
    reloadData();
  }, [surveyId, reloadData]);

  return (
    <>
      {isLoading ? (
        <LoadingComponent displayMode="flex" />
      ) : (
        surveyResult &&
        (surveyResult.isCompleted ? (
          <>
            <Seo title={surveyResult.body.title} img={surveyResult.surveyImage} />
            <CompletedPage content={surveyResult.completeMessage.content} title={surveyResult.completeMessage.title} />
          </>
        ) : (
          <>
            <Seo title={surveyResult.body.title} img={surveyResult.surveyImage} />
            <SurveySubmitForm
              surveyResult={{
                surveyId: surveyId,
                surveyBody: surveyResult.body,
                banner: surveyResult.surveyImage,
                consents: surveyResult.consents && surveyResult.consents.length > 0 ? surveyResult.consents : undefined,
                agentProfile: surveyResult.agentProfile,
              }}
            />
          </>
        ))
      )}
      {isError && <ErrorPage />}
    </>
  );
};
