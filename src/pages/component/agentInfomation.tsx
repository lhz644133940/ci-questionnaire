import React, { FC, useState, useEffect } from 'react';

type information = {
  agentName: string;
  agentTitle: string;
};

export const AgentInformation: FC<information> = ({ agentName, agentTitle }) => {
  const [width, setWidth] = useState<number>(window.innerWidth);

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange);
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange);
    };
  }, []);

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth);
  };

  const isMobile = width < 768 && navigator.userAgent.match(/iPad/i) === null;

  const imgTopBottomPadding = isMobile ? 16 : 30;
  const imgSidePadding = isMobile ? 16 : 50;
  const imgMaxWidth = isMobile ? 84 : 122;
  const imgMaxHeight = isMobile ? 38 : 55;

  return (
    <div style={{ marginBottom: '16px' }}>
      <div
        style={{
          width: '100%',
          maxHeight: 116,
          display: 'flex',
          flexDirection: 'row',
          backgroundColor: '#FFFFFF',
          boxShadow: '0px 4px 10px #00000019',
        }}
      >
        <div
          style={{
            width: '60%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'center',
            padding: '16px 0px 16px 16px',
          }}
        >
          <text
            style={{ fontStyle: 'normal', fontWeight: 600, fontSize: '16px', lineHeight: '24px', color: '#000000' }}
          >
            {agentName}
          </text>
          <text
            style={{ fontStyle: 'normal', fontWeight: 500, fontSize: '12px', lineHeight: '16px', color: '#999999' }}
          >
            {agentTitle}
          </text>
        </div>
        <div style={{ width: '40%', display: 'flex', justifyContent: 'flex-end' }}>
          <img
            src={`${require('../../images/agent-logo.png')}`}
            alt=""
            style={{
              maxWidth: imgMaxWidth,
              maxHeight: imgMaxHeight,
              padding: `${imgTopBottomPadding}px ${imgSidePadding}px`,
              objectFit: 'contain',
            }}
          />
        </div>
      </div>
    </div>
  );
};
