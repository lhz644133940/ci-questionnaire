import React, { FC } from 'react';
import { ErrorContainer } from '../questionnaire/types/containerStyle';

type errorContent = {
  displayMode: 'flex' | 'none'
}

export const ShowErrorComponent: FC<errorContent> = ({ displayMode}) => {
  return (
    <ErrorContainer style={{display: displayMode }}>
        <div className='loading'>
            <div className='loading-la-d1'>
                <div className='loading-cir-la-content'>
                    We’ve having trouble submitting the survey. Please reach your agent for more support.
                </div>
            </div>
        </div>
    </ErrorContainer>
  );
}

