import React from 'react';
import useHeight from '../../hook/useHeight/useHeight';

export const ErrorPage = () => {
  const pageHeight = useHeight();
  return (
    <div
      style={{
        height: `${pageHeight}px`,
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f5f5f5',
      }}
    >
      <img alt="" src={`${require('../../images/error.png')}`} style={{ height: '180px', width: '180px' }}></img>
      <text style={{ fontSize: '18px', fontWeight: 700, lineHeight: '18px', letterSpacing: 0, color: '#283542' }}>
        We are sorry
      </text>
      <text
        style={{
          margin: '84px',
          marginTop: '8px',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: 400,
          lineHeight: '19px',
          opacity: 0.9,
          color: '#5C6E80',
        }}
      >
        We’ve having trouble getting the survey. Please reach your agent for more support.
      </text>
    </div>
  );
};
