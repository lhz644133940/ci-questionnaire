import React, { FC } from 'react'
import DoneIcon from '@mui/icons-material/Done'
import useHeight from '../../hook/useHeight/useHeight'
import { CompleteMessage } from '../questionnaire/types/surveyTypes'
import { Container } from '../questionnaire/types/containerStyle'

export const CompletedPage: FC<CompleteMessage> = ({ content, title }) => {
  const height = useHeight()

  return (
    <Container>
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      <div style={{ height: `${height}px`, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
        <span
          style={{
            marginBottom: '24px',
            backgroundColor: '#24C791',
            border: '#24C791 1px solid',
            borderRadius: '72px',
          }}
        >
          <DoneIcon sx={{ color: 'white', fontSize: '72px' }}></DoneIcon>
        </span>
        <p
          style={{
            marginBottom: '4px',
            color: '#333333',
            fontFamily: 'SF Pro Text',
            fontSize: 18,
            fontWeight: 600,
            lineHeight: '26px',
          }}
        >
          {title || 'Thank you for your response'}
        </p>
        <p style={{ color: '#999999', fontFamily: 'SF Pro Text', fontSize: 14, fontWeight: 500, lineHeight: '20px' }}>
          {content || 'our urgent will contant you soon'}
        </p>
      </div>
    </Container>
  )
}
