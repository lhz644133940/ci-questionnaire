import React, { FC } from 'react';
import { LoadingContainer } from '../questionnaire/types/containerStyle';

type loadingContent = {
  content?: string;
  displayMode: 'flex' | 'none';
};

export const LoadingComponent: FC<loadingContent> = ({ content, displayMode }) => {
  return (
    <LoadingContainer style={{ display: displayMode }}>
      <div className="loading">
        <div className="loading-la-d1">
          <div className="loading-cir-la"></div>
          <div className="loading-cir-la-content">{(content && content) || 'Loading'}</div>
        </div>
      </div>
    </LoadingContainer>
  );
};
