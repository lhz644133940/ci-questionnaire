import React, { FC } from 'react';
import { Helmet } from 'react-helmet';

type props = {
  title: string;
  img?: string;
};

export const Seo: FC<props> = ({ title, img }) => {
  const imgPath = img || require('../../images/pru-logo.png');

  const meta: Array<{ property: string; content: string } | { name: string; content: string }> = [
    {
      property: 'og:title',
      content: title,
    },
    {
      name: 'twitter:title',
      content: title,
    },
    {
      name: 'al:title',
      content: title,
    },
    {
      name: 'twitter:image',
      content: imgPath,
    },
    {
      property: 'og:image',
      content: imgPath,
    },
    {
      name: 'al:image',
      content: imgPath,
    },
    {
      property: 'og:description',
      content: ' '
    }
  ];

  return <Helmet title={title} meta={meta} />;
};
