
import './index.css'

import React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { createTheme } from '@material-ui/core'

import App from './App'

const theme = createTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#292929',
    },
  },
})

const container = document.getElementById('root')
const root = createRoot(container!)
root.render(
  <MuiThemeProvider theme={theme}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </MuiThemeProvider>
)
