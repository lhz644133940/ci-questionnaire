import axios, { AxiosRequestConfig } from 'axios';
import { get } from 'lodash';

export default async function ResourceService<T>(
  isPost: boolean,
  url: string,
  body?: object,
  params?: object
): Promise<T> {
  const COMMON_PUBLIC_FRONTEND_ROOT_PATH = get(
    window,
    'envConfig.COMMON_PUBLIC_FRONTEND_ROOT_PATH',
    'https://phkl-apim-nprd.prudential.com.hk/pruforce/public/coedev'
  );
  console.log('COMMON_PUBLIC_FRONTEND_ROOT_PATH', COMMON_PUBLIC_FRONTEND_ROOT_PATH);
  const requestConfig: AxiosRequestConfig = {
    timeout: 60000,
    withCredentials: false,
    baseURL: COMMON_PUBLIC_FRONTEND_ROOT_PATH,
  };

  const apiClient = axios.create(requestConfig);

  apiClient.interceptors.response.use(
    data => {
      return data;
    },
    error => {
      if (!error.response) {
        throw error;
      }
      if (error.response.data) {
        throw error.response.data;
      } else {
        throw error.message;
      }
    }
  );

  const result = isPost
    ? await apiClient
        .post<T>(url, body, params)
        .then(response => {
          return response.data;
        })
        .catch(err => {
          throw err;
        })
    : await apiClient
        .get<T>(url)
        .then(response => response.data)
        .catch(err => {
          console.log('result', url);
          throw err;
        });

  return result;
}
