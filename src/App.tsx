import React from 'react';
import { QuestionnairePage } from './pages/questionnaire/questionnairePage';
import { Route } from 'react-router-dom';

export default function App() {
  return <Route path={`${process.env.PUBLIC_URL}/:surveyId`} component={QuestionnairePage}></Route>;
}
